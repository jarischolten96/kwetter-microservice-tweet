package kwetter.microservice.tweet.service;

import kwetter.microservice.tweet.models.entity.TweetDAO;
import kwetter.microservice.tweet.repository.ITweetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TweetService {
    private final ITweetRepository tweetRepository;

    public TweetDAO addTweet(TweetDAO tweet){
        return tweetRepository.save(tweet);
    }

    public List<TweetDAO> getTweetsById(UUID id){
        return tweetRepository.findAllByUserId(id).orElse(null);
    }
}
