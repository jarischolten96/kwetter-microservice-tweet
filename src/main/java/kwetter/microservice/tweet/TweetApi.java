package kwetter.microservice.tweet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TweetApi
{
    public static void main(String[] args)
    {
        SpringApplication.run(TweetApi.class, args);
    }

}