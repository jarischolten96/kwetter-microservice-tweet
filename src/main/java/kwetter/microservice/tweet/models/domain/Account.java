package kwetter.microservice.tweet.models.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private UUID id;
    private String email;
    private String userName;
    private String password;
    private String fullName;
    private String bio;
    private List<Account> followers;
    private List<Account> following;
    private List<Tweet> tweets;
}
