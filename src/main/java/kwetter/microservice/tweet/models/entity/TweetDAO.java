package kwetter.microservice.tweet.models.entity;

import kwetter.microservice.tweet.models.entity.converter.UUIDConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TweetDAO {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private UUID userId;
    private String text;
    @Column(name = "like_ids")
    @Convert(converter = UUIDConverter.class)
    private List<UUID> likeUUIDs;
    private Date date;
}
