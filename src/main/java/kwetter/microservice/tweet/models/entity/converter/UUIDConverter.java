package kwetter.microservice.tweet.models.entity.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Converter
public class UUIDConverter implements AttributeConverter<List<UUID>, String> {
        private final ObjectMapper objectMapper;

        public UUIDConverter() {
            this.objectMapper = new ObjectMapper();
        }

        @Override
        public String convertToDatabaseColumn(final List<UUID> map) {
            try {
                return objectMapper.writeValueAsString(map);
            }
            catch (JsonProcessingException ex) {
                ex.printStackTrace();
                return null;
                //TODO: throw iets
                //throw new ObjectToJSONException("Error in converting Map to JSON");
            }
        }

        @Override
        public List<UUID> convertToEntityAttribute(final String jsonMap) {
            try {
                return objectMapper.readValue(jsonMap, new TypeReference<List<UUID>>() {});
            }
            catch (IOException ex) {
                ex.printStackTrace();

                //throw new JSONToObjectException("Error in reading JSON as Map");
                //TODO: throw iets
                return null;
            }
        }
    }