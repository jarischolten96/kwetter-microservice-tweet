package kwetter.microservice.tweet.mapper;


import kwetter.microservice.tweet.models.domain.Account;
import kwetter.microservice.tweet.models.domain.Tweet;
import kwetter.microservice.tweet.models.entity.TweetDAO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class ObjectMapper {

    public TweetDAO toTweetDAO(Tweet tweet)
    {
        return TweetDAO.builder()
                .id(tweet.getId())
                .text(tweet.getText())
                .userId(tweet.getUser().getId())
                .date(tweet.getDate())
                .likeUUIDs(new ArrayList<>())
                .build();

    }

    public Tweet toTweet(TweetDAO tweetDAO)
    {
        return tweetDAO == null ? null : Tweet.builder()
                .id(tweetDAO.getId())
                .text(tweetDAO.getText())
                .user(Account.builder().id(tweetDAO.getUserId()).build())
                .likes(this.toAccountIdOnly(tweetDAO.getLikeUUIDs()))
                .build();
    }

    private List<Account> toAccountIdOnly(List<UUID> likeIds)
    {
        List<Account> accounts = new ArrayList<>();
        for (UUID likeId : likeIds)
        {
            accounts.add(Account.builder().id(likeId).build());
        }
        return accounts;
    }

    public List<Tweet> toTweetList(List<TweetDAO> tweetList)
    {
        List<Tweet> tweets = new ArrayList<>();
        for (TweetDAO tweet : tweetList)
        {
            tweets.add(this.toTweet(tweet));
        }
        return tweets;
    }

}
