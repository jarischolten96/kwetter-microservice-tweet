package kwetter.microservice.tweet.controller;

import com.google.gson.Gson;
import kwetter.microservice.tweet.mapper.ObjectMapper;
import kwetter.microservice.tweet.models.domain.Account;
import kwetter.microservice.tweet.models.domain.Tweet;
import kwetter.microservice.tweet.models.entity.TweetDAO;
import kwetter.microservice.tweet.service.TweetService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/tweet")
@RequiredArgsConstructor
public class TweetController {
    private final ObjectMapper objectMapper;
    private final TweetService tweetService;

    @PostMapping("/")
    public ResponseEntity<Tweet> addTweet(@RequestBody String tweet)
    {
        Tweet tweet2 = new Gson().fromJson(tweet,Tweet.class);
        TweetDAO returnedTweet = tweetService.addTweet(objectMapper.toTweetDAO(tweet2));
        return ResponseEntity.ok(objectMapper.toTweet(returnedTweet));
    }

    @GetMapping("/getTweetsByUserId/{id}")
    public ResponseEntity<List<Tweet>> getTweetsByUserId(@PathVariable("id") String id){
        List<Tweet> tweets = objectMapper.toTweetList(tweetService.getTweetsById(UUID.fromString(id)));
        return ResponseEntity.ok(tweets);
    }
}
