package kwetter.microservice.tweet.controller;

import kwetter.microservice.tweet.custom_exceptions.ChangeNotSupportedException;
import kwetter.microservice.tweet.custom_exceptions.UserDoesNotMatchBookingException;
import kwetter.microservice.tweet.custom_exceptions.CredentialsNotOkayException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HttpControllerAdvice extends ResponseEntityExceptionHandler
{
    @ExceptionHandler(UserDoesNotMatchBookingException.class)
    public ResponseEntity handleUserDoesNotMatchBookingException(final UserDoesNotMatchBookingException e)
    {
        log.error(String.format("UserDoesNotMatchBookingException 401, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @ExceptionHandler(ChangeNotSupportedException.class)
    public ResponseEntity handleChangeNotSupportedException(final ChangeNotSupportedException e)
    {
        log.error(String.format("ChangeNotSupportedException 400, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @ExceptionHandler(CredentialsNotOkayException.class)
    public ResponseEntity handleCredentialsNotOkayException(final CredentialsNotOkayException e)
    {
        log.error(String.format("CredentialsNotOkayException 400, %s at: %s", e.getMessage(), e.getStackTrace()[0]));
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}