package kwetter.microservice.tweet.repository;

import kwetter.microservice.tweet.models.entity.TweetDAO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ITweetRepository extends CrudRepository<TweetDAO, UUID> {
    Optional<List<TweetDAO>> findAllByUserId(final UUID uuid);
}
