package kwetter.microservice.tweet.service_response_exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ServiceBadRequestException extends RuntimeException {}
