package kwetter.microservice.tweet.service_response_exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ServiceUnhandledException extends RuntimeException {}
